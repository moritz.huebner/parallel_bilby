Parallel Bilby
==============

A python package to run gravitational wave inference analyses on multiple cores of a
machine using their message passing interface (MPI).

Online material to help you get started:

* `Installation instructions`_
* `Examples`_
* `Documentation`_
* `Parallel Bilby paper`_


.. _Documentation: https://lscsoft.docs.ligo.org/parallel_bilby/
.. _Installation instructions: https://lscsoft.docs.ligo.org/parallel_bilby/installation
.. _Examples: https://lscsoft.docs.ligo.org/parallel_bilby/examples
.. _Parallel Bilby paper: https://arxiv.org/pdf/1909.11873.pdf

