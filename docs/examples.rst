=============
Examples
=============

As Parallel Bilby is still under active development, the examples to use Parallel
Bilby are constantly under change. To get a working example, contact the authors of
Parallel Bilby (Gregory Ashton and Rory Smith) on the Parallel Bilby channel of the
`Bilby Slack Workspace`_.

.. _Bilby Slack Workspace: https://bilby-code.slack.com/